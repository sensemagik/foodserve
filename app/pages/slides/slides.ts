import {Page, Slides, Platform, NavController} from 'ionic-angular';
import {TabsPage} from '../tabs/tabs';

@Page({
  templateUrl: 'build/pages/slides/slides.html'
})

export class SlidePage {
    nav: any;
    platform: any;
    constructor(platform: Platform,nav: NavController){
    this.nav = nav;
    this.platform = platform;
    }
    
  mySlideOptions = {
    initialSlide: 0,
    pager: true,
    loop: true
  };
  
  openApp(){
    this.nav.push(TabsPage, {});
  }
  
}